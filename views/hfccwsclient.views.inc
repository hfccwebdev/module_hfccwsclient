<?php

/**
 * @file
 * Views handlers for hfccwsclient.module.
 */

/**
 * Implements hook_views_data_alter().
 */
function hfccwsclient_views_data_alter(&$data) {
  // if field_person_hank_id is defined on this site, add some
  // views field handlers to pull data from the staff directory
  // based on the person's id.

  $field_info = field_info_field('field_person_hank_id');
  if (!empty($field_info)) {
    $data['field_data_field_person_hank_id']['staffdir_username'] = array(
      'group' => t('Staff Directory'),
      'title' => t('HANK Username'),
      'help' => t("Return the person's username."),
      'real field' => 'field_person_hank_id_value',
      'field' => array(
        'handler' => 'hfccwsclient_handler_field_staffdir_username',
        'click sortable' => FALSE,
      ),
    );

    $data['field_data_field_person_hank_id']['staffdir_firstname'] = array(
      'group' => t('Staff Directory'),
      'title' => t('HANK First Name'),
      'help' => t("Return the person's first name."),
      'real field' => 'field_person_hank_id_value',
      'field' => array(
        'handler' => 'hfccwsclient_handler_field_staffdir_firstname',
        'click sortable' => FALSE,
      ),
    );

    $data['field_data_field_person_hank_id']['staffdir_lastname'] = array(
      'group' => t('Staff Directory'),
      'title' => t('HANK Last Name'),
      'help' => t("Return the person's last name."),
      'real field' => 'field_person_hank_id_value',
      'field' => array(
        'handler' => 'hfccwsclient_handler_field_staffdir_lastname',
        'click sortable' => FALSE,
      ),
    );

    $data['field_data_field_person_hank_id']['staffdir_credentials'] = array(
      'group' => t('Staff Directory'),
      'title' => t('Credentials'),
      'help' => t("Return the person's credentials."),
      'real field' => 'field_person_hank_id_value',
      'field' => array(
        'handler' => 'hfccwsclient_handler_field_staffdir_credentials',
        'click sortable' => FALSE,
      ),
    );

    $data['field_data_field_person_hank_id']['staffdir_title'] = array(
      'group' => t('Staff Directory'),
      'title' => t('HANK Title'),
      'help' => t("Return the person's title."),
      'real field' => 'field_person_hank_id_value',
      'field' => array(
        'handler' => 'hfccwsclient_handler_field_staffdir_title',
        'click sortable' => FALSE,
      ),
    );

    $data['field_data_field_person_hank_id']['staffdir_division'] = array(
      'group' => t('Staff Directory'),
      'title' => t('HANK Division'),
      'help' => t("Return the person's division."),
      'real field' => 'field_person_hank_id_value',
      'field' => array(
        'handler' => 'hfccwsclient_handler_field_staffdir_division',
        'click sortable' => FALSE,
      ),
    );

    $data['field_data_field_person_hank_id']['staffdir_department'] = array(
      'group' => t('Staff Directory'),
      'title' => t('HANK Department'),
      'help' => t("Return the person's department."),
      'real field' => 'field_person_hank_id_value',
      'field' => array(
        'handler' => 'hfccwsclient_handler_field_staffdir_department',
        'click sortable' => FALSE,
      ),
    );

    $data['field_data_field_person_hank_id']['staffdir_building'] = array(
      'group' => t('Staff Directory'),
      'title' => t('HANK Building'),
      'help' => t("Return the person's building."),
      'real field' => 'field_person_hank_id_value',
      'field' => array(
        'handler' => 'hfccwsclient_handler_field_staffdir_building',
        'click sortable' => FALSE,
      ),
    );

    $data['field_data_field_person_hank_id']['staffdir_office_location'] = array(
      'group' => t('Staff Directory'),
      'title' => t('HANK Office Location'),
      'help' => t("Return the person's office location."),
      'real field' => 'field_person_hank_id_value',
      'field' => array(
        'handler' => 'hfccwsclient_handler_field_staffdir_office_location',
        'click sortable' => FALSE,
      ),
    );

    $data['field_data_field_person_hank_id']['staffdir_phone_number'] = array(
      'group' => t('Staff Directory'),
      'title' => t('HANK Phone Number'),
      'help' => t("Return the person's phone number."),
      'real field' => 'field_person_hank_id_value',
      'field' => array(
        'handler' => 'hfccwsclient_handler_field_staffdir_phone_number',
        'click sortable' => FALSE,
      ),
    );

    $data['field_data_field_person_hank_id']['staffdir_email_address'] = array(
      'group' => t('Staff Directory'),
      'title' => t('HANK Email Address'),
      'help' => t("Return the person's email address."),
      'real field' => 'field_person_hank_id_value',
      'field' => array(
        'handler' => 'hfccwsclient_handler_field_staffdir_email_address',
        'click sortable' => FALSE,
      ),
    );

    $data['field_data_field_person_hank_id']['staffdir_suffix'] = array(
      'group' => t('Staff Directory'),
      'title' => t('Suffix'),
      'help' => t("Return the person's academic suffix(es)."),
      'real field' => 'field_person_hank_id_value',
      'field' => array(
        'handler' => 'hfccwsclient_handler_field_staffdir_suffix',
        'click sortable' => FALSE,
      ),
    );

    if (module_exists('hfc_hank_api')) {
      $data['field_data_field_person_hank_id']['id_photo'] = array(
        'group' => t('Staff Directory'),
        'title' => t('ID Photo'),
        'help' => t("Return the person's ID Photo."),
        'real field' => 'field_person_hank_id_value',
        'field' => array(
          'handler' => 'hfccwsclient_handler_field_staffdir_id_photo',
          'click sortable' => FALSE,
        ),
      );
    }
  }
}
