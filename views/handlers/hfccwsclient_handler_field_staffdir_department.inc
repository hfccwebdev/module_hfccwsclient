<?php

/**
 * Return the person's department.
 *
 * @ingroup views_field_handlers
 */
class hfccwsclient_handler_field_staffdir_department extends views_handler_field {

  /**
   * Render the field.
   */
  public function render($values) {
    $value = $this->get_value($values);
    if (!empty($value)) {
      if ($staffdir = WebServicesClient::getStaffdir(["id=$value", "all=true"])) {
        $staffdir = reset($staffdir);
        $departments = WebServicesClient::getDepartmentsOpts();
        return !empty($departments[$staffdir['department']]) ? $this->sanitize_value($departments[$staffdir['department']]) : NULL;
      }
    }
  }

}
