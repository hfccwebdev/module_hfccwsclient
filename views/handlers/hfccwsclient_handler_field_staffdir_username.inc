<?php

/**
 * Return the person's username.
 *
 * @ingroup views_field_handlers
 */
class hfccwsclient_handler_field_staffdir_username extends views_handler_field {

  /**
   * Define field options.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['link_to_staffdir'] = array(
      'default' => FALSE,
      'bool' => TRUE,
    );
    return $options;
  }

  /**
   * Provide link to staffdir option.
   */
  public function options_form(&$form, &$form_state) {
    $form['link_to_staffdir'] = array(
      '#title' => t('Link this field to the staff directory'),
      '#description' => t("Enable to override this field's links."),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_staffdir']),
    );

    parent::options_form($form, $form_state);
  }

  /**
   * Render the value as a link.
   */
  public function render_link($data) {
    if (!empty($this->options['link_to_staffdir']) && !empty($data)) {
      if ($data !== NULL && $data !== '') {
        $this->options['alter']['make_link'] = TRUE;
        $this->options['alter']['path'] = "staff-directory/" . $data;
      }
      else {
        $this->options['alter']['make_link'] = FALSE;
      }
    }
    return $data;
  }

  /**
   * Render the field.
   */
  public function render($values) {
    $value = $this->get_value($values);
    if (!empty($value)) {
      if ($staffdir = WebServicesClient::getStaffdir(["id=$value", "all=true"])) {
        $staffdir = reset($staffdir);
        return $this->render_link($this->sanitize_value($staffdir['username']));
      }
    }
  }

}
