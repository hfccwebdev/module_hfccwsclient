<?php

/**
 * Return the person's phone number.
 *
 * @ingroup views_field_handlers
 */
class hfccwsclient_handler_field_staffdir_phone_number extends views_handler_field {

  /**
   * Render the field.
   */
  public function render($values) {
    $value = $this->get_value($values);
    if (!empty($value)) {
      if ($staffdir = WebServicesClient::getStaffdir(["id=$value", "all=true"])) {
        $staffdir = reset($staffdir);
        return !empty($staffdir['phone_number']) ? $this->sanitize_value($staffdir['phone_number']) : NULL;
      }
    }
  }

}
