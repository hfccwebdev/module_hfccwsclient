<?php

/**
 * Return the person's email address.
 *
 * @ingroup views_field_handlers
 */
class hfccwsclient_handler_field_staffdir_email_address extends views_handler_field {

  /**
   * Render the field.
   */
  public function render($values) {
    $value = $this->get_value($values);
    if (!empty($value)) {
      if ($staffdir = WebServicesClient::getStaffdir(["id=$value", "all=true"])) {
        $staffdir = reset($staffdir);
        return !empty($staffdir['email_address']) ? $this->sanitize_value($staffdir['email_address']) : NULL;
      }
    }
  }

}
