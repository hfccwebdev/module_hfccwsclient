<?php

/**
 * Return the person's building.
 *
 * @ingroup views_field_handlers
 */
class hfccwsclient_handler_field_staffdir_building extends views_handler_field {

  /**
   * Define field options.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['code_only'] = array('default' => 0);
    return $options;
  }

  /**
   * Create the options form.
   */
  public function options_form(&$form, &$form_state) {
    $form['code_only'] = array(
      '#type' => 'radios',
      '#title' => t('Display options'),
      '#options' => array(1 => t('Building Code'), 0 => t('Building Name')),
      '#default_value' => $this->options['code_only'],
      '#description' => t('Select whether to display the building code or its full name.'),
    );
    parent::options_form($form, $form_state);
  }

  /**
   * Render the field.
   */
  public function render($values) {
    $value = $this->get_value($values);
    if (!empty($value)) {
      if ($staffdir = WebServicesClient::getStaffdir(array('id=' . $value))) {
        $staffdir = reset($staffdir);
        $code_only = !empty($this->options['code_only']) ? $this->options['code_only'] : 0;
        if (!empty($staffdir['building'])) {
          if ($code_only) {
            return $this->sanitize_value($staffdir['building']);
          }
          else {
            $buildings = WebServicesClient::getBuildingsOpts();
            return !empty($buildings[$staffdir['building']]) ? $buildings[$staffdir['building']] : $staffdir['building'];
          }
        }
      }
    }
  }

}
