<?php

/**
 * Return the person's credentials.
 *
 * @ingroup views_field_handlers
 */
class hfccwsclient_handler_field_staffdir_credentials extends views_handler_field {

  /**
   * Render the field.
   */
  public function render($values) {
    $value = $this->get_value($values);
    if (!empty($value)) {
      if ($staffdir = WebServicesClient::getStaffdir(["id=$value", "all=true"])) {
        $staffdir = reset($staffdir);
        return !empty($staffdir['credentials']) ? $this->sanitize_value($staffdir['credentials'], 'xss') : NULL;
      }
    }
  }

}
