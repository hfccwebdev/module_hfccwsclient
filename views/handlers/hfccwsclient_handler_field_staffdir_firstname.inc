<?php

/**
 * Return the person's first name.
 *
 * @ingroup views_field_handlers
 */
class hfccwsclient_handler_field_staffdir_firstname extends views_handler_field {

  /**
   * Render the field.
   */
  public function render($values) {
    $value = $this->get_value($values);
    if (!empty($value)) {
      if ($staffdir = WebServicesClient::getStaffdir(["id=$value", "all=true"])) {
        $staffdir = reset($staffdir);
        return !empty($staffdir['firstname']) ? $this->sanitize_value($staffdir['firstname']) : NULL;
      }
    }
  }

}
