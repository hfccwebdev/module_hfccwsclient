<?php

/**
 * Return the person's id photo.
 *
 * @ingroup views_field_handlers
 */
class hfccwsclient_handler_field_staffdir_id_photo extends views_handler_field {

  /**
   * Render the field.
   */
  public function render($values) {
    $value = $this->get_value($values);
    if (!empty($value) && is_numeric($value)) {
      $id = substr("0000000" . $value, -7, 7);
      if ($id_photo = HfcHankApi::getPhoto($id)) {
        return $id_photo;
      }
    }
  }

}
