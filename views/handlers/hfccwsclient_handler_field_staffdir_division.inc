<?php

/**
 * Return the person's division.
 *
 * @ingroup views_field_handlers
 */
class hfccwsclient_handler_field_staffdir_division extends views_handler_field {

  /**
   * Render the field.
   */
  public function render($values) {
    $value = $this->get_value($values);
    if (!empty($value)) {
      if ($staffdir = WebServicesClient::getStaffdir(["id=$value", "all=true"])) {
        $staffdir = reset($staffdir);
        $divisions = WebServicesClient::getDivisionsOpts();
        return !empty($divisions[$staffdir['division']]) ? $this->sanitize_value($divisions[$staffdir['division']]) : NULL;
      }
    }
  }

}
