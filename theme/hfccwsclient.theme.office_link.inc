<?php

/**
 * @file
 *
 * Contains them functions for Office Link.
 */

/**
 * Theme preprocess function for theme_hfc_office_link().
 */
function template_preprocess_hfc_office_link(&$variables, $hook) {
  $id = $variables['element']['office'];
  $offices = WebServicesClient::getOffices();
  if (!empty($offices[$id])) {
    $office = $offices[$id];
  }
  else {
    watchdog('hfccwsclient', 'Cannot theme unknown office %element', array('%element' => $variables['element']));
    $office = array('title' => NULL);
  }

  $variables['building'] = $office;

  $markup = !empty($office['website']) ? l($office['title'], check_url($office['website'])) : $office['title'];

  $variables['markup'] = $markup;
}

/**
 * Theme function for hfc_office_link.
 */
function theme_hfc_office_link(&$variables) {
  return !empty($variables['markup']) ? $variables['markup'] : NULL;
}
