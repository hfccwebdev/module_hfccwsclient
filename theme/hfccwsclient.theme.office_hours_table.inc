<?php

/**
 * @file
 * Contains them functions for Office Hours Table.
 */

/**
 * Theme preprocess function for theme_hfc_office_hours_table().
 */
function template_preprocess_hfc_office_hours_table(&$variables, $hook) {
  $office = $variables['element']['office'];
  $current_term = WebServicesClient::getCurrentTerm();
  $current_term = !empty($current_term) ? drupal_substr($current_term, -2, 2) : NULL;

  $rows = [];
  $saturday = FALSE;
  $sunday = FALSE;
  foreach (['Fall', 'Winter', 'Summer'] as $term) {
    $term_field = drupal_strtolower($term) . '_hours';
    if (!empty($office[$term_field])) {

      // Initialize the table row to set the column title and ensure correct spacing.
      // Start with columns for semester, then Monday through Friday.
      // We will add columns for Saturday and Sunday if needed.
      $row = [
        'data' => [['data' => $term, 'class' => ['term-name']], NULL, NULL, NULL, NULL, NULL],
        'class' => [strtr($term_field, '_', '-')],
      ];

      // Add current-hours class to appropriate row.
      if (drupal_substr(drupal_strtoupper($term), 0, 2) == $current_term) {
        $row['class'][] = 'current-term';
      }

      $found = FALSE;

      $days = [];
      foreach ($office[$term_field] as $segment) {
        if (!empty($segment['day']) && !empty($segment['starthours']) && !empty($segment['endhours'])) {
          $days[$segment['day']][] = WebServicesClient::formatTime($segment['starthours']) . ' - ' . WebServicesClient::formatTime($segment['endhours']);
        }
      }

      if (!empty($days)) {
        ksort($days);
        foreach ($days as $day => $times) {
          switch ($day) {
            case '0':
              $saturday = TRUE;
              $sunday = TRUE;
              $row['data'][7] = implode('<br>', $times);
              break;

            case '6':
              $saturday = TRUE;

            default:
              $row['data'][$day] = implode('<br>', $times);
          }
        }
        $rows[$term_field] = $row;
      }
    }
  }

  $headers = [
    ['data' => NULL, 'class' => ['term-name']],
    t('Monday'),
    t('Tuesday'),
    t('Wednesday'),
    t('Thursday'),
    t('Friday'),
  ];
  if ($saturday) {
    $headers[] = t('Saturday');
  }
  if ($sunday) {
    $headers[] = t('Sunday');
  }

  $variables['content'] = [
    '#theme' => 'table',
    '#header' => $headers,
    '#rows' => $rows,
    '#sticky' => FALSE,
    '#attributes' => array('class' => array('hfccwsclient-office-hours')),
    '#empty' => t('No schedule found for this office.'),
  ];

}

/**
 * Theme function for hfc_office_hours_table.
 */
function theme_hfc_office_hours_table(&$variables) {
  return !empty($variables['content']) ? render($variables['content']) : NULL;
}
