<?php

/**
 * @file
 * Contains them functions for HANK Buildings.
 */

/**
 * Theme preprocess function for theme_hank_building().
 */
function template_preprocess_hank_building(&$variables, $hook) {
  $buildings = WebServicesClient::getBuildings();
  $building = $variables['element']['building'];

  if (!empty($buildings[$building])) {
    $building = $buildings[$building];
  }
  else {
    watchdog('hfccwsclient', 'Cannot theme unknown building name %element', array('%element' => $building));
    $building = array(
      'buildings_id' => $variables['element']['building'],
      'bldg_desc' => $variables['element']['building'],
    );
  }

  $variables['building'] = $building;

  if (isset($variables['element']['#valcodes']) && $variables['element']['#valcodes']) {
    $markup = t('!desc (!id)', array('!id' => $building['buildings_id'], '!desc' => $building['bldg_desc']));
  }
  else {
    $markup = $building['bldg_desc'];
  }
  $variables['markup'] = $markup;
}

/**
 * Theme function for hank_building.
 */
function theme_hank_building(&$variables) {
  return !empty($variables['markup']) ? $variables['markup'] : NULL;
}
