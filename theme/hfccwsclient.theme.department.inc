<?php

/**
 * @file
 * Contains them functions for HANK Departments.
 */

/**
 * Theme preprocess function for theme_hank_department().
 */
function template_preprocess_hank_department(&$variables, $hook) {
  $departments = WebServicesClient::getDepartments();
  $department = $variables['element']['department'];
  $nolink = !empty($variables['element']['#nolink']) ? $variables['element']['#nolink'] : FALSE;

  if (!empty($departments[$department])) {
    $department = $departments[$department];
  }
  else {
    watchdog('hfccwsclient', 'Cannot theme unknown department name %dept', array('%dept' => $department));
    $department = array('depts_desc' => $variables['element']);
  }

  $variables['department'] = $department;

  if (!empty($department['url']) && $nolink !== TRUE) {
    $markup = l($department['depts_desc'], $department['url']);
  }
  else {
    $markup = $department['depts_desc'];
  }
  $variables['markup'] = $markup;
}

/**
 * Theme function for hank_department.
 */
function theme_hank_department(&$variables) {
  return !empty($variables['markup']) ? $variables['markup'] : NULL;
}
