<?php

/**
 * @file
 * Contains them functions for hfc_office_programs.
 */

/**
 * Theme preprocess function for theme_hfc_office_programs().
 *
 * @see template_preprocess_field()
 * @see template_preprocess_hfcc_global_pseudo_field()
 */
function template_preprocess_hfc_office_programs(&$variables, $hook) {
  $office = $variables['element']['nid'];

  $programs = WebServicesClient::getPrograms();

  $rows = array();

  foreach ($programs as $program) {
    if (!empty($program['office_id']) && $program['office_id'] == $office) {
      $rows[] = l(decode_entities($program['title']), $program['path']);
    }
  }
  $content = array(array(
    '#theme' => 'item_list',
    '#items' => $rows,
  ));

  $variables['content'] = $content;
}

/**
 * Theme function for getpersoninfo_authtree_info.
 *
 * @todo move this to a .tpl.php file.
 */
function theme_hfc_office_programs(&$variables) {
  return !empty($variables['content']) ? render($variables['content']) : NULL;
}
