<?php

/**
 * @file
 * Contains them functions for hfc_office_contact.
 */

/**
 * Theme preprocess function for theme_hfc_office_contact().
 *
 * @see template_preprocess_field()
 * @see template_preprocess_hfcc_global_pseudo_field()
 */
function template_preprocess_hfc_office_contact(&$variables, $hook) {
  $item = $variables['element'];

  $content = array(
    '#prefix' => '<div class="officeinfo">',
    '#suffix' => '</div>',
  );

  if (!empty($item['website'])) {
    $content[] = array(
      '#field_name' => 'hfccwsclient_office_website',
      '#type' => 'item',
      '#label' => t('Website'),
      '#label_display' => 'inline',
      '#markup' => l(check_url($item['website']), check_url($item['website'])),
      '#theme' => 'hfcc_global_pseudo_field',
    );
  }

    if (!empty($item['email'])) {
    $content[] = array(
      '#field_name' => 'hfccwsclient_office_email',
      '#type' => 'item',
      '#label' => t('Email'),
      '#label_display' => 'inline',
      '#markup' => $item['email'],
      '#theme' => 'hfcc_global_pseudo_field',
    );
  }

  if (!empty($item['phone_number'])) {
    $content[] = array(
      '#field_name' => 'hfccwsclient_office_phone',
      '#type' => 'item',
      '#label' => t('Phone Number'),
      '#label_display' => 'inline',
      '#markup' => $item['phone_number'],
      '#theme' => 'hfcc_global_pseudo_field',
    );
  }

  if (!empty($item['fax'])) {
    $content[] = array(
      '#field_name' => 'hfccwsclient_office_fax',
      '#type' => 'item',
      '#label' => t('Fax'),
      '#label_display' => 'inline',
      '#markup' => $item['fax'],
      '#theme' => 'hfcc_global_pseudo_field',
    );
  }

  $location = array();
  if (!empty($item['building'])) {
    $building = array('building' => $item['building'], '#theme' => 'hank_building');
    $location[] = render($building);
  }
  if (!empty($item['office_location'])) {
    $location[] = $item['office_location'];
  }
  if (!empty($location)) {
    $content[] = array(
      '#field_name' => 'hfccwsclient_office_location',
      '#type' => 'item',
      '#label' => t('Location'),
      '#label_display' => 'inline',
      '#markup' => implode(', ', $location),
      '#theme' => 'hfcc_global_pseudo_field',
    );
  }

  if (!empty($item['administrator'])) {
    $id = $item['administrator'];
    $staffdir = WebServicesClient::getStaffdir(array('id=' . $id));
    if (!empty($staffdir)) {
      $administrator = $staffdir[$id]['firstname'] . ' ' . $staffdir[$id]['lastname'];
      $content[] = array(
        '#field_name' => 'hfccwsclient_administrator',
        '#type' => 'item',
        '#label' => t('Administrator'),
        '#label_display' => 'inline',
        '#markup' => $administrator,
        '#theme' => 'hfcc_global_pseudo_field',
      );
    }
    else {
      drupal_set_message(t('Could not look up administrator contact info for @title.', array('@title' => $item['title'])), 'warning');
      watchdog('hfccwsclient', 'Could not look up administrator %id for office %title.', array('%id' => $id, '%title' => $item['title']), WATCHDOG_WARNING);
    }
  }

  // Yes, I went there.
  $hours = ['office' => $item, '#theme' => 'hfc_office_hours_table'];
  $content[] = ['#markup' => render($hours)];

  $variables['content'] = $content;
}

/**
 * Theme function for hfc_office_contact.
 *
 * @todo move this to a .tpl.php file.
 */
function theme_hfc_office_contact(&$variables) {
  return !empty($variables['content']) ? render($variables['content']) : NULL;
}
