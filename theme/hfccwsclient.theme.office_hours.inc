<?php

/**
 * @file
 * Contains them functions for hfc_office_hours.
 */

/**
 * Theme preprocess function for theme_hfc_office_hours().
 *
 * @see template_preprocess_field()
 * @see template_preprocess_hfcc_global_pseudo_field()
 */
function template_preprocess_hfc_office_hours(&$variables, $hook) {
  $item = $variables['element'];

  $content = array(
    '#prefix' => '<div class="officeinfo">',
    '#suffix' => '</div>',
  );

  $content[] = array(
    '#prefix' => '<h4>',
    '#markup' => isset($item['website']) ? l($item['title'], check_url($item['website'])) : $item['title'],
    '#suffix' => '</h4>',
  );

  $content[] = array(
    '#field_name' => 'hfccwsclient_hours_today',
    '#type' => 'item',
    '#label' => t('Hours Today'),
    '#label_display' => 'hidden',
    '#markup' => WebServicesClient::getOfficeHoursToday($item),
    '#theme' => 'hfcc_global_pseudo_field',
  );

  $location = array();
  if (!empty($item['building'])) {
    $building = array('building' => $item['building'], '#theme' => 'hank_building');
    $location[] = render($building);
  }
  if (!empty($item['office_location'])) {
    $location[] = $item['office_location'];
  }
  if (!empty($location)) {
    $content[] = array(
      '#field_name' => 'hfccwsclient_office_location',
      '#type' => 'item',
      '#label' => t('Location'),
      '#label_display' => 'hidden',
      '#markup' => implode(', ', $location),
      '#theme' => 'hfcc_global_pseudo_field',
    );
  }

  if (!empty($item['phone_number'])) {
    $content[] = array(
      '#field_name' => 'hfccwsclient_office_phone',
      '#type' => 'item',
      '#label' => t('Phone Number'),
      '#label_display' => 'hidden',
      '#markup' => $item['phone_number'],
      '#theme' => 'hfcc_global_pseudo_field',
    );
  }

  $variables['content'] = $content;
}

/**
 * Theme function for getpersoninfo_authtree_info.
 *
 * @todo move this to a .tpl.php file.
 */
function theme_hfc_office_hours(&$variables) {
  return !empty($variables['content']) ? render($variables['content']) : NULL;
}
