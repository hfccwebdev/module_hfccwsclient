<?php

/**
 * @file
 * Contains them functions for HANK Course.
 */

/**
 * Implements theme_preprocess().
 *
 * @see template_preprocess_field()
 */
function template_preprocess_hfccwsclient_course(&$variables) {
  $course = $variables['element'];
  $content = [];

  if (!empty($course['credit_hours'])) {
    $content[] = array(
      '#field_name' => 'display_course_credit_hours',
      '#label' => t('Credit Hours'),
      '#label_display' => 'inline',
      '#markup' => $course['credit_hours'],
      '#theme' => 'hfcc_global_pseudo_field',
    );
  }

  if (!empty($course['description'])) {
    $content[] = array(
      '#field_name' => 'display_course_description',
      '#label' => t('Course Description'),
      '#label_display' => 'above',
      '#markup' => $course['description'],
      '#theme' => 'hfcc_global_pseudo_field',
    );
  }

  if (!empty($course['prerequisites'])) {
    $content[] = array(
      '#field_name' => 'display_course_prerequisites',
      '#label' => t('Prerequisites'),
      '#label_display' => 'inline',
      '#markup' => $course['prerequisites'],
      '#theme' => 'hfcc_global_pseudo_field',
    );
  }

  if (!empty($course['corequisites'])) {
    $content[] = array(
      '#field_name' => 'display_course_corequisites',
      '#label' => t('Co-requisites'),
      '#label_display' => 'inline',
      '#markup' => $course['corequisites'],
      '#theme' => 'hfcc_global_pseudo_field',
    );
  }

  if (!empty($course['cpcll'])) {
    $content[] = array(
      '#field_name' => 'display_course_cpcll',
      '#label' => t('Prior College-level Learning Credit'),
      '#label_display' => 'inline',
      '#markup' => t('This course is eligible for credit for prior college-level learning. Please contact the division for details.'),
      '#theme' => 'hfcc_global_pseudo_field',
    );
  }

  if (!empty($course['note'])) {
    $content[] = array(
      '#field_name' => 'display_course_note',
      '#label' => t('Note'),
      '#label_display' => 'inline',
      '#markup' => $course['note'],
      '#theme' => 'hfcc_global_pseudo_field',
    );
  }

  $variables['content'] = $content;

  $variables['title'] = t('@subject-@number @title', array(
      '@subject' => $course['subject'],
      '@number' => $course['number'],
      '@title' => $course['title'],
    )
  );

  $variables['page'] = !empty($course['#page']) ? $course['#page'] : FALSE;

  $classes_array = array('hfccwsclient-course');
  $classes_array[] = drupal_strtolower('hfccwsclient-course-' . $course['subject'] . '-' . $course['number']);
  $classes_array[] = 'clearfix';
  $variables['classes_array'] = $classes_array;

}
