<?php

/**
 * @file
 * Contains them functions for HANK Divisions.
 */

/**
 * Theme preprocess function for theme_hank_division().
 */
function template_preprocess_hank_division(&$variables, $hook) {
  $divisions = WebServicesClient::getDivisions();
  $division = $variables['element']['division'];
  $nolink = !empty($variables['#element']['#nolink']) ? $variables['#element']['#nolink'] : FALSE;

  if (!empty($divisions[$division])) {
    $division = $divisions[$division];
  }
  else {
    watchdog('hfccwsclient', 'Cannot theme unknown division name %div', array('%div' => $division));
    $division = array('div_desc' => $variables['element']);
  }

  $variables['division'] = $division;

  if (!empty($division['url']) && $nolink !== TRUE) {
    $markup = l($division['div_desc'], $division['url']);
  }
  else {
    $markup = $division['div_desc'];
  }
  $variables['markup'] = $markup;
}

/**
 * Theme function for hank_division.
 */
function theme_hank_division(&$variables) {
  return !empty($variables['markup']) ? $variables['markup'] : NULL;
}
