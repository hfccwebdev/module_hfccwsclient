<?php

/**
 * @file
 * Template implementation to display an HFCC Catalog Course.
 *
 * @ingroup themeable
 */
?>
<div class="<?php print implode(' ', $classes_array); ?>">
  <?php if (!$page): ?>
    <h4 class="title"><?php print $title; ?></h4>
  <?php endif; ?>
  <div class="content">
    <?php print render($content); ?>
  </div>
</div>
