<?php

/**
 * @file
 * Contains drush commands provided by this module.
 */

/**
 * Implements hook_drush_command().
 */
function hfccwsclient_drush_command() {
  return [
    'staffdir-missing' => [
      'description' => 'Generate a report of missing field_person_hank_id references.',
    ],
    'staffdir-export' => [
      'description' => 'Export the employee directory in CSV format.',
      'options' => [
        'all' => 'Include all positions.'
      ],
    ],
  ];
}

/**
 * Callback for the drush staffdir-missing command.
 */
function drush_hfccwsclient_staffdir_missing() {
  echo StaffdirTools::create()->staffdirMissingRefReport();
}

/**
 * Callback for the drush staffdir-export command.
 */
function drush_hfccwsclient_staffdir_export() {
  $options = [];
    if (drush_get_option('all', FALSE)) {
      $options['all'] = 'true';
    }
  echo StaffdirTools::create()->export($options);
}
