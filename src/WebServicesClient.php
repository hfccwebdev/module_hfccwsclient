<?php

/**
 * The HFC Web Services Client Interface.
 *
 * @ingroup hfcc_modules
 */
interface WebServicesClientInterface {

  /**
   * Create an instance of this class.
   */
  public static function create();

  /**
   * Retrieve a list of buildings from the services host.
   */
  public static function getBuildings();

  /**
   * Retrieve the full name for the specified building.
   */
  public static function getBuildingName($buildings_id);

  /**
   * Retrieve buildings as a list of options.
   */
  public static function getBuildingsOpts($allow_all = FALSE);

  /**
   * Retrieve catalog courses from the services host.
   */
  public static function getCourses($filter_opts = []);

  /**
   * Retrieve courses as a list of options.
   */
  public static function getCoursesOpts($filter_opts = []);

  /**
   * Retrieve catalog courses from the services host.
   */
  public static function getCourseSections($filter_opts = []);

  /**
   * Retrieve a list of departments from the services host.
   */
  public static function getDepartments();

  /**
   * Retrieve a list of divisions from the services host.
   */
  public static function getDivisions();

  /**
   * Retrieve divisions as a list of options.
   */
  public static function getDivisionsOpts($allow_all = FALSE);

  /**
   * Retrieve global navigation links from the services host.
   */
  public static function getNavigation();

  /**
   * Retrieve global navigation taxonomy terms from the services host.
   */
  public static function getNavigationMenus();

  /**
   * Retrieve offices from the services host.
   */
  public static function getOffices($filter_opts = []);

  /**
   * Retrieve offices as a list of options.
   */
  public static function getOfficesOpts($allow_all = FALSE);

  /**
   * Determine today's office hours.
   */
  public static function getOfficeHoursToday($office);

  /**
   * Retrieve the legacy program ID redirects list.
   */
  public static function getProgramRedirects();

  /**
   * Retrieve catalog program degree types from the services host.
   */
  public static function getProgramTypes();

  /**
   * Retrieve catalog programs from the services host.
   */
  public static function getPrograms();

  /**
   * Retrieve Programs as a list of options.
   */
  public static function getProgramsOpts($allow_all = FALSE);

  /**
   * Retrieve a list of subjects from the services host.
   */
  public static function getSubjects();

  /**
   * Retrieve staff directory from the services host.
   */
  public static function getStaffdir($filter_opts = []);

  /**
   * Retrieve staff directory by HANK IDs as options list.
   */
  public static function getStaffdirHankidOpts($filter_opts = []);

  /**
   * Retrieve a list of academic terms from the services host.
   */
  public static function getTerms();

  /**
   * Retrieve the full name for the specified term.
   */
  public static function getTermName($tid);

  /**
   * Retrieve terms as a list of options.
   */
  public static function getTermsOpts($filter_opts = [], $allow_all = FALSE);

  /**
   * Retrieve the current terms_id.
   */
  public static function getCurrentTerm();

  /**
   * Retrieve the current academic year.
   */
  public static function getCurrentAcYr();

  /**
   * Format office hours times.
   * @see office_hours.module
   */
  public static function formatTime($time);

  /**
   * Helper function to convert '800' or '0800' to '08:00'.
   */
  public static function time24hr($time = '');

  /**
   * Retrieve a URL from the services host.
   */
  public static function httpRequest($resource, $filter_opts = []);

}

/**
 * The HFC Web Services Client.
 *
 * @ingroup hfcc_modules
 */
class WebServicesClient implements WebServicesClientInterface {

  /**
   * Filter for UG terms only.
   *
   * usage: preg_match(self::UG_TERM_PATTERN, $term['terms_id'])
   */
  const UG_TERM_PATTERN = '!^[0-9]./(FA|WI|SP|SU)$!';

  /**
   * {@inheritdoc}
   */
  public static function create() {
    return new static();
  }

  /**
   * {@inheritdoc}
   */
  public static function getBuildings() {
    $output = &drupal_static(__FUNCTION__);
    if (!isset($output)) {
      if ($cache = cache_get('hfccwsclient_buildings')) {
        $output = $cache->data;
      }
      else {
        if ($buildings = self::httpRequest('buildings')) {
          $output = [];
          foreach ($buildings as $building) {
            $output[$building['buildings_id']] = $building;
          }
          cache_set('hfccwsclient_buildings', $output, 'cache', CACHE_TEMPORARY);
        }
        else {
          drupal_set_message(t('Could not retrieve buildings list.'));
          return FALSE;
        }
      }
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public static function getBuildingName($buildings_id) {
    $buildings = self::getBuildings();
    return !empty($buildings[$buildings_id]) ? $buildings[$buildings_id]['bldg_desc'] : $buildings_id;
  }

  /**
   * {@inheritdoc}
   */
  public static function getBuildingsOpts($allow_all = FALSE) {
    $buildings = self::getBuildings();
    $options = [];
    foreach ($buildings as $id => $building) {
      $options[$id] = $building['bldg_desc'];
    }
    if ($allow_all) {
      $options['All'] = '- All -';
    }
    asort($options);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public static function getCourses($filter_opts = []) {
    $args = !empty($filter_opts) ? '?' . implode('&', $filter_opts) : NULL;
    $cid = 'hfccwsclient_courses_' . hash("SHA256", $args);
    if (!isset($output)) {
      if ($cache = cache_get($cid)) {
        $output = $cache->data;
      }
      else {
        if ($courses = self::httpRequest('courses', $filter_opts)) {
          $output = [];
          if (isset($courses['#messages']['count']) && $courses['#messages']['count'] == "0") {
            watchdog('hfccwsclient', 'No courses matched %args', ['%args' => implode('&', $filter_opts)], WATCHDOG_WARNING);
          }
          foreach (element_children($courses) as $key) {
            $output[$key] = $courses[$key];
          }
          cache_set($cid, $output, 'cache', CACHE_TEMPORARY);
        }
        else {
          drupal_set_message(t('Could not retrieve requested course listings.'));
          return FALSE;
        }
      }
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public static function getCoursesOpts($filter_opts = []) {
    $courses = self::getCourses($filter_opts);
    $options = [];
    foreach ($courses as $key => $item) {
      $options[$key] = t('@subject @number @title', [
        '@subject' => $item['subject'],
        '@number' => $item['number'],
        '@title' => $item['title'],
      ]);
    }
    asort($options);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public static function getCourseSections($filter_opts = []) {
    $args = !empty($filter_opts) ? '?' . implode('&', $filter_opts) : NULL;
    $cid = 'hfccwsclient_course_sections_' . hash("SHA256", $args);
    if (!isset($output)) {
      if ($cache = cache_get($cid)) {
        $output = $cache->data;
      }
      else {
        if ($sections = self::httpRequest('course-sections', $filter_opts)) {
          $output = [];
          foreach ($sections as $section) {
            $output[drupal_strtolower(str_replace("/", "", $section['sec_term']) . '-' . $section['crs_name'] . '-' . $section['sec_no'])] = $section;
          }
          cache_set($cid, $output, 'cache', CACHE_TEMPORARY);
        }
        else {
          drupal_set_message(t('Could not retrieve requested course sections listing.'));
          return FALSE;
        }
      }
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public static function getDepartments() {
    $output = &drupal_static(__FUNCTION__);
    if (!isset($output)) {
      if ($cache = cache_get('hfccwsclient_departments')) {
        $output = $cache->data;
      }
      else {
        if ($departments = self::httpRequest('departments')) {
          $output = [];
          foreach ($departments as $department) {
            $output[$department['depts_id']] = $department;
          }
          cache_set('hfccwsclient_departments', $output, 'cache', CACHE_TEMPORARY);
        }
        else {
          drupal_set_message(t('Could not retrieve departments list.'));
          return FALSE;
        }
      }
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public static function getDepartmentsOpts($allow_all = FALSE) {
    $departments = self::getDepartments();
    $options = [];
    foreach ($departments as $id => $department) {
      $options[$id] = $department['depts_desc'];
    }
    if ($allow_all) {
      $options['All'] = '- All -';
    }
    asort($options);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public static function getDivisions() {
    $output = &drupal_static(__FUNCTION__);
    if (!isset($output)) {
      if ($cache = cache_get('hfccwsclient_divisions')) {
        $output = $cache->data;
      }
      else {
        if ($divisions = self::httpRequest('divisions')) {
          $output = [];
          foreach ($divisions as $division) {
            $output[$division['divisions_id']] = $division;
          }
          cache_set('hfccwsclient_divisions', $output, 'cache', CACHE_TEMPORARY);
        }
        else {
          drupal_set_message(t('Could not retrieve divisions list.'));
          return FALSE;
        }
      }
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public static function getDivisionsOpts($allow_all = FALSE) {
    if ($divisions = self::getDivisions()) {
      $options = [];
      foreach ($divisions as $id => $division) {
        $options[$id] = $division['div_desc'];
      }
      if ($allow_all) {
        $options['All'] = '- All -';
      }
      asort($options);
      return $options;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getNavigation() {
    watchdog('hfccwsclient', 'Catalog-based navigation is no longer supported.', [], WATCHDOG_ERROR);
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public static function getNavigationMenus() {
    watchdog('hfccwsclient', 'Catalog-based navigation is no longer supported.', [], WATCHDOG_ERROR);
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public static function getOffices($filter_opts = []) {
    $args = !empty($filter_opts) ? '?' . implode('&', $filter_opts) : NULL;
    $cid = 'hfccwsclient_offices' . hash("SHA256", $args);
    if ($cache = cache_get($cid)) {
      $output = $cache->data;
    }
    else {
      if ($data = self::httpRequest('offices', $filter_opts)) {
        $output = [];
        foreach ($data as $item) {
          $output[$item['nid']] = $item;
        }
        cache_set($cid, $output, 'cache', CACHE_TEMPORARY);
      }
      else {
        drupal_set_message(t('Could not retrieve office contact information.'));
        return FALSE;
      }
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public static function getOfficesOpts($allow_all = FALSE) {
    $offices = self::getOffices();
    $options = [];
    foreach ($offices as $id => $office) {
      $options[$id] = $office['title'];
    }
    if ($allow_all) {
      $options['All'] = '- All -';
    }
    asort($options);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public static function getOfficeHoursToday($office) {
    $terms = [
      'WI' => 'winter_hours',
      'SU' => 'summer_hours',
      'FA' => 'fall_hours',
    ];
    $term = drupal_substr(self::getCurrentTerm(), -2);
    $weekday = date('N');

    $hours = $office[$terms[$term]];
    if (empty($hours)) {
      return;
    }
    $today = [];
    foreach ($hours as $segment) {
      if ($segment['day'] == $weekday && !empty($segment['starthours']) && !empty($segment['endhours'])) {
        $today[] = self::formatTime($segment['starthours']) . ' - ' . self::formatTime($segment['endhours']);
      }
    }
    if (!empty($today)) {
      return t('Open today: @times', ['@times' => implode(', ', $today)]);
    }
    else {
      return t('Closed today.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getProgramRedirects() {
    watchdog('hfccwsclient', 'Legacy program redirects are no longer supported.', [], WATCHDOG_ERROR);
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public static function getProgramTypes() {
    $output = &drupal_static(__FUNCTION__);
    if (!isset($output)) {
      if ($cache = cache_get('hfccwsclient_program_types')) {
        $output = $cache->data;
      }
      else {
        if ($programs = self::getPrograms()) {
          $output = [];
          foreach ($programs as $item) {
            $output[$item['degree_type_raw']] = $item['degree_type'];
            asort($output);
          }
          cache_set('hfccwsclient_program_types', $output, 'cache', CACHE_TEMPORARY);
        }
        else {
          drupal_set_message(t('Could not retrieve degree types.'));
          return FALSE;
        }
      }
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public static function getPrograms() {
    $output = &drupal_static(__FUNCTION__);
    if (!isset($output)) {
      if ($cache = cache_get('hfccwsclient_programs')) {
        $output = $cache->data;
      }
      else {
        if ($programs = self::httpRequest('programs')) {
          $output = [];
          foreach ($programs as $program) {
            $output[$program['nid']] = $program;
          }
          cache_set('hfccwsclient_programs', $output, 'cache', CACHE_TEMPORARY);
        }
        else {
          drupal_set_message(t('Could not retrieve program listings.'));
          return FALSE;
        }
      }
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public static function getProgramsOpts($allow_all = FALSE) {
    $programs = self::getPrograms();
    $options = [];
    foreach ($programs as $id => $program) {
      $options[$id] = $program['title'] . " (" . $program['degree_type'] . ")";
    }
    if ($allow_all) {
      $options['All'] = '- All -';
    }
    asort($options);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubjects() {
    $output = &drupal_static(__FUNCTION__);
    if (!isset($output)) {
      if ($cache = cache_get('hfccwsclient_subjects')) {
        $output = $cache->data;
      }
      else {
        if ($subjects = self::httpRequest('subjects')) {
          $output = [];
          foreach ($subjects as $subject) {
            $output[$subject['subjects_id']] = $subject['subj_desc'];
          }
          cache_set('hfccwsclient_subjects', $output, 'cache', CACHE_TEMPORARY);
        }
        else {
          drupal_set_message(t('Could not retrieve subjects list.'));
          return FALSE;
        }
      }
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public static function getStaffdir($filter_opts = []) {
    $output = [];
    $args = !empty($filter_opts) ? '?' . implode('&', $filter_opts) : NULL;
    $cid = 'hfccwsclient_staffdir_' . hash("SHA256", $args);
    if ($cache = cache_get($cid)) {
      $output = $cache->data;
    }
    else {
      if ($staffdir = self::httpRequest('staffdir', $filter_opts)) {
        if (isset($staffdir['#messages']['count']) && $staffdir['#messages']['count'] == "0") {
          watchdog('hfccwsclient', 'No employees matched %args', ['%args' => implode('&', $filter_opts)], WATCHDOG_WARNING);
        }
        foreach (element_children($staffdir) as $key) {
          $output[$key] = $staffdir[$key];
        }
        cache_set($cid, $output, 'cache', CACHE_TEMPORARY);
      }
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public static function getStaffdirHankidOpts($filter_opts = []) {
    $output = [];
    $args = !empty($filter_opts) ? '?' . implode('&', $filter_opts) : NULL;
    $cid = 'hfccwsclient_staffopts_' . hash("SHA256", $args);
    if ($cache = cache_get($cid)) {
      $output = $cache->data;
    }
    else {
      if ($staffdir = self::httpRequest('staffopts', $filter_opts)) {
        if (isset($staffdir['#messages']['count']) && $staffdir['#messages']['count'] == "0") {
          watchdog('hfccwsclient', 'No employees matched %args', ['%args' => implode('&', $filter_opts)], WATCHDOG_WARNING);
        }
        foreach (element_children($staffdir) as $key) {
          $output[$key] = $staffdir[$key];
        }
        cache_set($cid, $output, 'cache', CACHE_TEMPORARY);
      }
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public static function getTerms() {
    $output = &drupal_static(__FUNCTION__);
    if (!isset($output)) {
      if ($cache = cache_get('hfccwsclient_terms')) {
        $output = $cache->data;
      }
      else {
        if ($terms = self::httpRequest('terms')) {
          $output = [];
          foreach (element_children($terms) as $key) {
            $output[$terms[$key]['terms_id']] = $terms[$key];
          }
          cache_set('hfccwsclient_terms', $output, 'cache', CACHE_TEMPORARY);
        }
        else {
          drupal_set_message(t('Could not retrieve terms list.'));
          return FALSE;
        }
      }
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public static function getTermName($tid) {
    $terms = self::getTerms();
    return !empty($terms[$tid]) ? $terms[$tid]['term_desc'] : $tid;
  }

  /**
   * {@inheritdoc}
   */
  public static function getTermsOpts($filter_opts = [], $allow_all = FALSE) {
    $terms = self::getTerms();

    $start_date = !empty($filter_opts['start_date']) ? $filter_opts['start_date'] : time() - 157680000;
    // todo: Add support for $end_date
    // todo: Add support for $term_type in ALL, /C, /T, /P, /R

    $options = [];
    if ($allow_all) {
      $options[''] = '- Select -';
    }
    foreach ($terms as $id => $term) {
      if ((empty($start_date) || $term['term_start_date'] > $start_date) && (preg_match(self::UG_TERM_PATTERN, $term['terms_id']))) {
        $options[$id] = $term['term_desc'];
      }
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public static function getCurrentTerm() {
    $terms = self::getTerms();
    foreach ($terms as $id => $term) {
      if ($term['term_start_date'] <= time() && $term['term_end_date'] >= time() && preg_match(self::UG_TERM_PATTERN, $term['terms_id'])) {
        return $id;
      }
    }
    // If we are between terms, no matches will be found above. Try again with term_reg_start_date.
    foreach ($terms as $id => $term) {
      if ($term['term_reg_start_date'] <= time() && $term['term_end_date'] >= time() && preg_match(self::UG_TERM_PATTERN, $term['terms_id'])) {
        return $id;
      }
    }
    // If we still did not match, throw an error message.
    watchdog('hfccwsclient', 'Could not determine current academic term.', [], WATCHDOG_ERROR);
  }

  /**
   * {@inheritdoc}
   */
  public static function getCurrentAcYr() {
    $tid = self::getCurrentTerm();
    list ($year, $term) = explode('/', $tid);
    return $term == 'FA' ? 2000 + intval($year) : 1999 + intval($year);
  }

  /**
   * {@inheritdoc}
   * @see office_hours.module
   */
  public static function formatTime($time) {
    try {
        $date = new DateTime(self::time24hr($time));
        $text = $date->format('g:i a');
        $text = preg_replace(['/am$/', '/pm$/'], ['a.m.', 'p.m.'], $text);
        return $text;
    } catch (Exception $e) {
      watchdog('hfccwsclient', 'Could not parse time %t', ['%t' => $time], WATCHDOG_ERROR);
      return $time;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function time24hr($time = '') {
    if (strstr($time, ':')) {
      return $time;
    }
    else {
      $time = substr('0000' . $time, -4);
      $hour = substr($time, 0, -2);
      $min = substr($time, -2);
      return $hour . ':' . $min;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function httpRequest($resource, $filter_opts = []) {
    if (!$api_url = variable_get('hfccwsclient_api_url', NULL)) {
      drupal_set_message(t('Please configure the web services URL before using this function.'), 'error');
      return FALSE;
    }
    $filter_opts[] = '_format=json';
    $options = [];
    if ($path = request_path()) {
      $referer = $GLOBALS['base_url'] . url($path);
      $options['headers'] = ['referer' => $referer];
    }

    if ($token = variable_get('hfc-catalog-api-token', NULL)) {
      $options['headers']['Authorization'] = "Bearer $token";
    }

    $request_url = "/{$resource}?" . implode('&', $filter_opts);
    $response = drupal_http_request($api_url . $request_url, $options);
    if ($response->code == '200' && !empty($response->data)) {
      return drupal_json_decode($response->data);
    }
    else {
      if (!empty($response->error)) {
        $message = 'A web services error occurred. (@code) @error';
        $values = ['@code' => $response->code, '@error' => $response->error];
        watchdog('hfccwsclient', $message, $values, WATCHDOG_ERROR);
        watchdog('hfccwsclient', 'Error occurred reading api @url', ['@url' => $request_url]);
        drupal_set_message(t($message, $values), 'error');
      }
      return FALSE;
    }
  }
}
