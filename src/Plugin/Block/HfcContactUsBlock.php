<?php

/**
 * Defines the Contact Us block.
 */
class HfcContactUsBlock extends HfcGlobalBaseBlock {

  /**
   * WebServicesClientInterface definition.
   *
   * @var WebServicesClientInterface
   */
  private $client;

  /**
   * {@inheritdoc}
   */
  public static function create() {
    $client = WebServicesClient::create();
    return new static($client);
  }

  /**
   * Class constructor.
   *
   * @param WebServicesClientInterface $client
   *   The web services client.
   */
  public function __construct(WebServicesClientInterface $client) {
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public function info() {
    return [
      'info' => t("Web Services Contact Us"),
      'cache' => DRUPAL_CACHE_GLOBAL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function build(&$output) {
    $buildings = WebServicesClient::getBuildings();
    $offices = WebServicesClient::getOffices();

    if (is_array($offices)) {
      $output = [];
      foreach ($offices as $item) {
        $row = [
          '#prefix' => '<div class="office-wrapper office-' . $item['nid'] . '">',
          '#suffix' => '</div>' . PHP_EOL,
        ];

        $row['office_info'] = [
          '#prefix' => '<div class="office-info">',
          '#suffix' => '</div>',
        ];

        $row['office_info']['office_name'] = [
          '#prefix' => '<h2 class="office-name">',
          '#markup' => !empty($item['website']) ? l($item['title'], check_url($item['website'])) : $item['title'],
          '#suffix' => '</h2>',
        ];

        if (!empty($item['phone_number'])) {
          $row['office_info']['office_phone'] = [
            '#prefix' => '<div class="office-phone">',
            '#markup' => t('Phone Number: @phone', ['@phone' => $item['phone_number']]),
            '#suffix' => '</div>',
          ];
        }

        if (!empty($item['email'])) {
          $row['office_info']['office_email'] = [
            '#prefix' => '<div class="office-email">',
            '#markup' => t('Email: <a href="mailto:@mail">@mail</a>', ['@mail' => $item['email']]),
            '#suffix' => '</div>',
          ];
        }

        $location = [];
        if (!empty($item['building'])) {
          if (!empty($buildings[$item['building']])){
            $location[] = $buildings[$item['building']]['bldg_desc'];
          }
          else {
            $location[] = $item['building'];
          }
        }
        if (!empty($item['office_location'])) {
          $location[] = $item['office_location'];
        }
        if (!empty($location)) {
          $row['office_info']['office_location'] = [
            '#prefix' => '<div class="office-location">',
            '#markup' => t('Location: @location', ['@location' => implode(', ', $location)]),
            '#suffix' => '</div>',
          ];
        }

        if (!empty($item['administrator'])) {
          $id = $item['administrator'];
          $staffdir = WebServicesClient::getStaffdir(array('id=' . $id));
          if (!empty($staffdir)) {
            $administrator = $staffdir[$id]['firstname'] . ' ' . $staffdir[$id]['lastname'];
            $row['office_info']['office_admin'] = [
              '#prefix' => '<div class="office-admin">',
              '#markup' => t('Administrator: @admin', ['@admin' => $administrator]),
              '#suffix' => '</div>',
            ];
          }
          else {
            watchdog(
              'hfccwsclient',
              'Could not look up administrator %id for office %title.',
              ['%id' => $id, '%title' => $item['title']],
              WATCHDOG_WARNING
            );
          }
        }

        $row['collapsible_header'] = [
          '#prefix' => '<div class="collapsible-title expand-plus" role="button" tabindex="0" aria-expanded="false">',
          '#markup' => t('@title hours and details', ['@title' => $item['title']]),
          '#suffix' => '</div>',
        ];

        $row['collapsible'] = [
          '#prefix' => '<div class="hfc-show-content">',
          '#suffix' => '</div>',
        ];

        if (!empty($item['fax']) || !empty($item['note'])) {

          $row['collapsible']['extra'] = [
            '#prefix' => '<div class="office-extra-info">',
            '#suffix' => '</div>',
          ];

          if (!empty($item['phone_number'])) {
            $row['collapsible']['extra']['office_fax'] = [
              '#prefix' => '<div class="office-fax">',
              '#markup' => t('Fax Number: @fax', ['@fax' => $item['fax']]),
              '#suffix' => '</div>',
            ];
          }

          if (!empty($item['note'])) {
            $row['collapsible']['extra']['office_note'] = [
              '#prefix' => '<div class="office-note">',
              '#markup' => check_markup($item['note'], filter_default_format()),
              '#suffix' => '</div>',
            ];
          }
        }
        $row['collapsible']['office_hours'] = [
          '#prefix' => '<div class="office-hours">',
          'office' => $item,
          '#theme' => 'hfc_office_hours_table',
          '#suffix' => '</div>',
        ];

        $output[] = $row;
      }
    }
  }

}
