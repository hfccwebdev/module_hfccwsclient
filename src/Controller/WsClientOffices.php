<?php

/**
 * @file
 * Display Office Hours and Contact pages for HFC Web Services Client.
 *
 * @see hfccwsclient.module
 */

class WsClientOffices {

  /**
   * Page callback for the contact list.
   */
  public static function contactPage() {
    $offices = WebServicesClient::getOffices();
    if (is_array($offices)) {
      $rows = array(
        array(t('Main Number'), '313-845-9600', NULL, NULL),
        array(t('Main Toll-Free Number'), '800-585-4322', NULL, NULL),
      );
      foreach ($offices as $item) {
        if (!empty($item['website'])) {
          $title = l($item['title'], check_url($item['website']));
        }
        else {
          $title = $item['title'];
        }
        $phone = !empty($item['phone_number']) ? $item['phone_number'] : NULL;
        $email = !empty($item['email']) ? l($item['email'], 'mailto:' . $item['email']) : NULL;
        $fax = !empty($item['fax']) ? $item['fax'] : NULL;
        $rows[] = array($title, $phone, $email, $fax);
      }
      return array(
        '#theme' => 'table',
        '#header' => array(
          t('Office'),
          t('Phone Number'),
          t('Email'),
          t('Fax Number'),
        ),
        '#rows' => $rows,
        '#attributes' => array('class' => array('contact-list')),
        '#empty' => t('No contact information found. Please try again later.'),
      );
    }
    else {
      return t('An error occurred while retrieving the office listing. Please try again later.');
    }
  }

  /**
   * Page callback for the hours of operation list.
   */
  public static function hoursPage() {
    $buildings = WebServicesClient::getBuildings();
    $offices = WebServicesClient::getOffices();
    if (is_array($offices)) {
      $output = array();
      foreach ($offices as $item) {
        $row = array(
          '#prefix' => '<div class="officeinfo">',
          '#suffix' => '</div>',
        );
        if (!empty($item['website'])) {
          $title = l($item['title'], check_url($item['website']));
        }
        else {
          $title = $item['title'];
        }
        $row[] = array(
          '#prefix' => '<h3 class="title">',
          '#markup' => $title,
          '#suffix' => '</h3>',
        );

        if (!empty($item['phone_number'])) {
          $row[] = array(
            '#field_name' => 'hfccwsclient_office_phone',
            '#type' => 'item',
            '#label' => t('Phone Number'),
            '#label_display' => 'inline',
            '#markup' => $item['phone_number'],
            '#theme' => 'hfcc_global_pseudo_field',
          );
        }

        $location = array();
        if (!empty($item['building'])) {
          if (!empty($buildings[$item['building']])){
            $location[] = $buildings[$item['building']]['bldg_desc'];
          }
          else {
            $location[] = $item['building'];
          }
        }
        if (!empty($item['office_location'])) {
          $location[] = $item['office_location'];
        }
        if (!empty($location)) {
          $row[] = array(
            '#field_name' => 'hfccwsclient_office_location',
            '#type' => 'item',
            '#label' => t('Location'),
            '#label_display' => 'inline',
            '#markup' => implode(', ', $location),
            '#theme' => 'hfcc_global_pseudo_field',
          );
        }

        if (!empty($item['administrator'])) {
          $id = $item['administrator'];
          $staffdir = WebServicesClient::getStaffdir(array('id=' . $id));
          if (!empty($staffdir)) {
            $administrator = $staffdir[$id]['firstname'] . ' ' . $staffdir[$id]['lastname'];
            $row[] = array(
              '#field_name' => 'hfccwsclient_administrator',
              '#type' => 'item',
              '#label' => t('Administrator'),
              '#label_display' => 'inline',
              '#markup' => $administrator,
              '#theme' => 'hfcc_global_pseudo_field',
            );
          }
          else {
            drupal_set_message(t('Could not look up administrator contact info for @title.', array('@title' => $item['title'])), 'warning');
            watchdog('hfccwsclient', 'Could not look up administrator %id for office %title.', array('%id' => $id, '%title' => $item['title']), WATCHDOG_WARNING);
          }
        }

        $row[] = array(
          'office' => $item,
          '#theme' => 'hfc_office_hours_table',
        );

        if (!empty($item['note'])) {
          $row[] = array(
            '#field_name' => 'hfccwsclient_office_note',
            '#label' => t('Note'),
            '#label_display' => 'hidden',
            '#markup' => check_markup($item['note'], filter_default_format()),
            '#theme' => 'hfcc_global_pseudo_field',
          );
        }

        $output[] = $row;
      }
      return $output;
    }
    else {
      return t('An error occurred while retrieving the office listing. Please try again later.');
    }
  }
}
