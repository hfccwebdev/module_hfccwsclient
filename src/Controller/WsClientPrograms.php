<?php

/**
 * @file
 * Display Catalog Programs pages for HFC Web Services Client.
 *
 * @see hfccwsclient.module
 */

class WsClientPrograms {

  /**
   * Page callback for programs listing.
   */
  public static function indexPage() {
    drupal_set_title(t('Programs of Study'));

    $programs = WebServicesClient::getPrograms();

    if (is_array($programs)) {
      $breadcrumbs = [l(t('Home'), '<front>')];
      if (drupal_valid_path('catalog')) {
        $breadcrumbs[] = l(t('Catalog'), 'catalog');
      }
      drupal_set_breadcrumb($breadcrumbs);

      $rows = [];
      foreach ($programs as $id => $program) {
        $office = !empty($program['office']) ? $program['office'] : NULL;
        $rows[] = [
          ['data' => l(decode_entities($program['title']), $program['path']), 'class' => array('title')],
          ['data' => $office, 'class' => array('division')],
          ['data' => $program['degree_type'], 'class' => array('degree-type')],
        ];
      }
      return [
        '#theme' => 'table',
        '#header' => [t('Program'), t('Division'), t('Type of program')],
        '#rows' => $rows,
        '#attributes' => ['class' => array('programs-list')],
        '#empty' => t('No programs found. Please try again later.'),
      ];
    }
    else {
      return t('An error occurred while retrieving the programs listing. Please try again later.');
    }
  }

  /**
   * Page callback to display a program.
   */
  public static function redirect($nid) {
    $programs = WebServicesClient::getProgramRedirects();
    if (!empty($programs[$nid])) {
      drupal_goto($programs[$nid]);
    }
    else {
      drupal_not_found();
      drupal_exit();
    }
  }
}
