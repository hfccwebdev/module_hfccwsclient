<?php

/**
 * @file
 * Display Catalog Courses pages for HFC Web Services Client.
 *
 * @see hfccwsclient.module
 */

class WsClientCourses {

  /**
   * Page callback for course detail.
   */
  public static function redirect($id) {

    watchdog('hfccwsclient', 'Catalog course redirect for @id', ['@id' => $id], WATCHDOG_INFO);

    $path = [variable_get('hfccwsclient_redirect_base', NULL), 'courses', $id];
    $path = join('/', $path);
    drupal_goto($path, [], '301');
  }
}
