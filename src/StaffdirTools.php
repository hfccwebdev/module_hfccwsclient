<?php

/**
 * Defines tools for displaying and using HFC Employee Directory data.
 */
class StaffdirTools {

  /**
   * WebServicesClientInterface definition.
   *
   * @var WebServicesClientInterface
   */
  private $client;

  /**
   * Create an instance of this class.
   */
  public static function create() {
    $client = WebServicesClient::create();
    return new static($client);
  }

  /**
   * Class constructor.
   *
   * @param WebServicesClientInterface $client
   *   The web services client.
   */
  public function __construct(WebServicesClientInterface $client) {
    $this->client = $client;
  }

  /**
   * Display a staff list for the default division.
   *
   * @todo Figure out if this is still used.
   * @todo Implement more display_opts().
   *
   * @deprecated and should no longer be used.
   */
  public static function getList($filter_opts = array(), $display_opts = array()) {

    @trigger_error('StaffdirTools::getList() is deprecated and should not be used.', E_USER_DEPRECATED);

    // Set default filter opts if none are passed.
    if (empty($filter_opts)) {
      $staffdiv = variable_get('hfccwsclient_staffdir_div', NULL);
      if (!empty($staffdiv) && $staffdiv !== 'All') {
        $filter_opts[] = 'division=' . $staffdiv;
      }
    }

    $staffdir = WebServicesClient::getStaffdir($filter_opts);
    $rows = array();
    foreach ($staffdir as $person) {
      $row = array(
        '#prefix' => '<li class="person">',
        '#suffix' => '</li>',
      );
      if (!empty($display_opts['last-name-first'])) {
        $fullname = !empty($person['lastname']) ? $person['lastname'] : '';
        $fullname .= !empty($person['firstname']) ? ', ' . $person['firstname'] : '';
      }
      else {
        $fullname = !empty($person['firstname']) ? $person['firstname'] : '';
        $fullname .= !empty($person['lastname']) ? ' ' . $person['lastname'] : '';
      }
      if (empty($display_opts['no-link']) && !empty($person['username'])) {
        $name = l($fullname, 'staff/' . $person['username']);
      }
      else {
        $name = $fullname;
      }
      $row['name'] = array(
        '#markup' => $name,
      );

      if (!empty($person['title'])) {
        $row['title'] = array(
          '#prefix' => '<br/>',
          '#markup' => $person['title'],
        );
      }

      if (!empty($person['email_address'])) {
        $row['email_address'] = array(
          '#prefix' => '<br/>',
          '#markup' => $person['email_address'],
        );
      }

      if (!empty($person['phone_number'])) {
        $row['phone_number'] = array(
          '#prefix' => '<br/>',
          '#markup' => $person['phone_number'],
        );
      }

      $rows[] = $row;
    }
    $output = array(
      '#prefix' => '<ul class="staff-list">',
      'content' => $rows,
      '#suffix' => '</ul>',
    );
    return $output;
  }

  /**
   * Get departments options for staff directory form.
   *
   * Filters unpopulated options out of regular list.
   */
  public static function getActiveDepartmentOpts($allow_all = FALSE) {
    if ($cache = cache_get('hfccwsclient_staffdir_departments_opts')) {
      return $cache->data;
    }
    else {
      $options = WebServicesClient::getDepartmentsOpts();
      $staffdir = WebServicesClient::getStaffdir();
      if (function_exists('array_column')) {
        $departments = array_column($staffdir, 'department');
      }
      else {
        $departments = array_map(function($element){return $element['department'];}, $staffdir);
      }
      foreach ($options as $key => $value) {
        if (!in_array($key, $departments)) {
          unset($options[$key]);
        }
      }
      if ($allow_all) {
        $options['All'] = '- All -';
        asort($options);
      }
      cache_set('hfccwsclient_staffdir_departments_opts', $options, 'cache', CACHE_TEMPORARY);
      return $options;
    }
  }

  /**
   * Get divisions options for staff directory form.
   *
   * Filters unpopulated options out of regular list.
   */
  public static function getActiveDivisionOpts($allow_all = FALSE) {
    if ($cache = cache_get('hfccwsclient_staffdir_divisions_opts')) {
      return $cache->data;
    }
    else {
      $options = WebServicesClient::getDivisionsOpts($allow_all);
      $staffdir = WebServicesClient::getStaffdir();
      if (function_exists('array_column')) {
        $divisions = array_column($staffdir, 'division');
      }
      else {
        $divisions = array_map(function($element){return $element['division'];}, $staffdir);
      }
      foreach ($options as $key => $value) {
        if (!in_array($key, $divisions)) {
          unset($options[$key]);
        }
      }
      if ($allow_all) {
        $options['All'] = '- All -';
        asort($options);
      }
      cache_set('hfccwsclient_staffdir_divisions_opts', $options, 'cache', CACHE_TEMPORARY);
      return $options;
    }
  }

  /**
   * Create a report of staffdir missing entity reference targets.
   */
  public function staffdirMissingRefReport() {
    $site_name = variable_get('site_name');
    $output = ["Checking for missing Staff Directory References for $site_name.", ""];

    $staffdir = $this->client->getStaffdirHankidOpts(["all=true"]);
    $references = $this->getHankPersonFieldReferences();

    $found = FALSE;

    foreach ($references as $hank_id => $node) {
      if (!isset($staffdir[$hank_id])) {
        $values = [
          '@id' => substr('0000000' . $hank_id, -7),
          '@type' => node_type_get_name($node->type),
          '@title' => $node->title,
          '@nid' => url('node/' . $node->nid, ['absolute' => TRUE]),
        ];
        $output[] = t("* @id missing for @type [@title](@nid)", $values);
        $found = TRUE;
      }
    }

    if (!$found) {
      $output[] = "No missing HANK references found.";
    }

    return PHP_EOL . implode(PHP_EOL, $output) . PHP_EOL;
  }

  /**
   * Export contents of employee directory as CSV.
   *
   * @param array $options
   *   Filter options to pass to Web Services Client.
   */
  public function export($options) {
    $staffdir = $this->client->getStaffdir($options);
    $rows = [];
    foreach ($staffdir as $person) {
      $rows[] = implode(', ', [
        $person['hank_id'],
        $person['username'],
        $person['lastname'],
        $person['firstname'],
        $person['division'],
        $person['department'],
        $person['barg_unit'],
        $person['title'],
      ]);
    }
    return implode(PHP_EOL, $rows) . PHP_EOL;
  }

  /**
   * Retrieve field usage and node information.
   */
  private function getHankPersonFieldReferences() {
    $query = db_select('field_data_field_person_hank_id', 'h');
    $query->addField('h', 'field_person_hank_id_value', 'hank_id');
    $query->join('node', 'n', "h.revision_id = n.vid AND h.entity_type = 'node'");
    $query->fields('n', ['title', 'nid', 'type']);
    $query->orderBy('n.title');
    return $query->execute()->fetchAllAssoc('hank_id');
  }
}
