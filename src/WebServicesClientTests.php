<?php

/**
 * @file
 * Includes tests for the web services client.
 */

/**
 * Web Services Client Tests
 */
class WebServicesClientTests {

  /**
   * Instantiate a new test.
   *
   * @param string $endpoint
   *   The services endpoint to test.
   *
   * @return array
   *   A renderable array.
   */
  public static function test($service) {
    $test = new WebServicesClientTests();
    return $test->execute($service);
  }

  /**
   * Constructor.
   */
  public function execute($service) {
    $method = $service . 'Test';
    if (method_exists($this, $method)) {
       cache_clear_all("hfccwsclient_$service", 'cache', TRUE);
      return $this->$method();
    }
  }

  /**
   * Test buildings.
   */
  private function buildingsTest() {
    if ($data = WebServicesClient::getBuildings()) {
      return [
        '#theme' => 'table',
        '#header' => [t('Code'), t('Name'), t('Campus'), t('Export to Mobile')],
        '#rows' => $data,
        '#empty' => t('No results returned'),
      ];
    }
  }

  /**
   * Test courses.
   */
  private function coursesTest() {
    if ($courses = WebServicesClient::getCourses()) {
      $rows = [];
      foreach ($courses as $id => $course) {
        $rows[] = [
          $course['subject'] . ' ' . $course['number'],
          decode_entities($course['title']),
          decode_entities($course['short_title']),
          $course['credit_hours'],
        ];
      }
      return [
        '#theme' => 'table',
        '#header' => [t('Course Number'), t('Course Title'), t('Short Title'), t('Credit Hours')],
        '#rows' => $rows,
        '#empty' => t('No results returned'),
      ];
    }
  }

  /**
   * Test departments.
   */
  private function departmentsTest() {
    if ($data = WebServicesClient::getDepartments()) {
      return [
        '#theme' => 'table',
        '#header' => [t('Code'), t('Name'), t('Unit'), t('URL'), t('Head'), t('Office')],
        '#rows' => $data,
        '#empty' => t('No results returned'),
      ];
    }
  }

  /**
   * Test divisions.
   */
  private function divisionsTest() {
    if ($data = WebServicesClient::getDivisions()) {
      return [
        '#theme' => 'table',
        '#header' => [t('Code'), t('Name'), t('Unit'), t('URL'), t('Head'), t('Office')],
        '#rows' => $data,
        '#empty' => t('No results returned'),
      ];
    }
  }

  /**
   * Test offices.
   */
  private function officesTest() {
    if ($data = WebServicesClient::getOffices()) {
      // return ['#prefix' => '<pre>', '#markup' => print_r($data, 1), '#suffix' => '</pre>'];
      $rows = [];
      foreach ($data as $item) {
        $title = !empty($item['website']) ? l($item['title'], $item['website']) : $item['title'];
        $rows[] = [
          $title,
          $item['division'],
          $item['department'],
          $item['administrator'],
          $item['building'],
          $item['office_location'],
          $item['email'],
          $item['phone_number'],
          $item['fax'],
        ];
      }
      return [
        '#theme' => 'table',
        '#header' => [t('Office'), t('Division'), t('Department'), t('Administrator'), t('Building'), t('Location'), t('Email'), t('Phone number'), t('Fax')],
        '#rows' => $rows,
        '#empty' => t('No results returned'),
      ];
    }
  }


  /**
   * Test programs.
   */
  private function programsTest() {
    if ($data = WebServicesClient::getPrograms()) {
      $rows = [];
      foreach ($data as $item) {
        $rows[] = [
          $item['title'],
          $item['degree_type_raw'],
          $item['degree_type'],
          $item['office'],
          $item['program_master'],
          $item['inactive'],
        ];
      }
      return [
        '#theme' => 'table',
        '#header' => [t('Title'), t('Degree Type Code'), t('Degree Type'), t('Office'), t('Program Master'), t('Inactive')],
        '#rows' => $rows,
        '#empty' => t('No results returned'),
      ];
    }
  }

  /**
   * Test staffdir.
   */
  private function staffdirTest() {
    $filter_opts = array();
    $staffdiv = variable_get('hfccwsclient_staffdir_div', NULL);
    if (!empty($staffdiv) && $staffdiv !== 'All') {
      $filter_opts[] = 'division=' . $staffdiv;
    }
    $staffdir = WebServicesClient::getStaffdir($filter_opts);
    $rows = array();
    foreach ($staffdir as $person) {
      $rows[] = array(
        !empty($person['username']) ? l($person['username'], 'staff-directory/' . $person['username']) : '',
        !empty($person['lastname']) ? $person['lastname'] : '',
        !empty($person['firstname']) ? $person['firstname'] : '',
        !empty($person['division']) ? $person['division'] : '',
        !empty($person['department']) ? $person['department'] : '',
        !empty($person['title']) ? $person['title'] : '',
        !empty($person['email_address']) ? $person['email_address'] : '',
        !empty($person['office_location']) ? $person['office_location'] : '',
        !empty($person['phone_number']) ? $person['phone_number'] : '',
      );
    }
    return array(
      '#theme' => 'table',
      '#header' => array(
        t('Username'),
        t('Last name'),
        t('First name'),
        t('Division'),
        t('Department'),
        t('Position'),
        t('Email'),
        t('Office'),
        t('Phone'),
      ),
      '#rows' => $rows,
      '#empty' => t('No results returned'),
    );
  }

  /**
   * Test subjects.
   */
  private function subjectsTest() {
    if ($data = WebServicesClient::getSubjects()) {
      $rows = [];
      foreach ($data as $key => $item) {
        $rows[] = [$key, $item];
      }
      return [
        '#theme' => 'table',
        '#header' => array(t('Code'), t('Name')),
        '#rows' => $rows,
        '#empty' => t('No results returned'),
      ];
    }
  }

  /**
   * Test terms.
   */
  private function termsTest() {
    if ($data = WebServicesClient::getTerms()) {
      $rows = [];
      foreach ($data as $key => $item) {
        $rows[] = [
          $key,
          $item['term_desc'],
          $item['term_reg_start_date'] > 0 ? format_date($item['term_reg_start_date'], 'custom', 'm/d/Y') : NULL,
          $item['term_start_date'] > 0 ? format_date($item['term_start_date'], 'custom', 'm/d/Y') : NULL,
          $item['term_add_end_date'] > 0 ? format_date($item['term_add_end_date'], 'custom', 'm/d/Y') : NULL,
          $item['term_drop_end_date'] > 0 ? format_date($item['term_drop_end_date'], 'custom', 'm/d/Y') : NULL,
          $item['term_end_date'] > 0 ? format_date($item['term_end_date'], 'custom', 'm/d/Y') : NULL,
        ];
      }
      return [
        '#theme' => 'table',
        '#header' => [
          t('ID'),
          t('Name'),
          t('Reg Start Date'),
          t('Term Start Date'),
          t('Add End Date'),
          t('Drop End Date'),
          t('Term End Date'),
        ],
        '#rows' => $rows,
        '#empty' => t('No results returned'),
      ];
    }
  }

}
