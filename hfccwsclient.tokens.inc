<?php

/**
 * @file
 * Token callbacks for the hfccwsclient module.
 */

/**
 * Implements hook_token_info().
 */
function hfccwsclient_token_info() {

  $types = [];
  $tokens = [];

  // Course description tokens.
  $types['course-description'] = [
    'name' => t('Course Descriptions'),
    'description' => t('Tokens related to course description data.'),
    'needs-data' => 'course-description',
    'field' => TRUE,
  ];
  $tokens['course-description'] = [
    'title' => [
      'name' => t('Course title'),
      'description' => t('The course title.'),
    ],
    'number_title' => [
      'name' => t('Course number and title'),
      'description' => t('The course number and title.'),
    ],
  ];

  // Tokens for node->field_person_hank_id
  $tokens['node'] = [
    'field_person_hank_fullname' => [
      'name' => t('HANK Full Name'),
      'description' => t('Full name from the Catalog Employee Directory.'),
    ],
    'field_person_hank_username' => [
      'name' => t('HANK Username'),
      'description' => t('Username from the Catalog Employee Directory.'),
    ],
  ];

  return [
    'types' => $types,
    'tokens' => $tokens,
  ];
}

/**
 * Implements hook_token_info_alter().
 *
 * @see addressfield_token_info_alter()
 */
function hfccwsclient_token_info_alter(&$info) {
  // Loop over every course_description field on the site.
  $course_description_fields = array_filter(field_info_field_map(), 'hfccwsclient_course_description_field_map_filter');
  foreach ($course_description_fields as $field_name => $field) {
    foreach ($info['tokens'] as $group => $token) {
      if (isset($info['tokens'][$group][$field_name]) && is_array($info['tokens'][$group][$field_name])) {
        // Set the token type for the field to use the course-description child tokens.
        $info['tokens'][$group][$field_name]['type'] = 'course-description';
      }
    }
  }
}

/**
 * Implements hook_tokens().
 */
function hfccwsclient_tokens($type, $tokens, array $data = [], array $options = []) {
  $replacements = [];
  if ($type == 'node') {
    foreach ($tokens as $name => $original) {

      // Process course_description field sub-tokens.
      // Requires extra code because $name needs to be parsed.

      if ($pos = strpos($name, ':number_title')) {
        $token = 'number_title';
        $field_name = substr($name, 0, $pos);
      }
      elseif ($pos = strpos($name, ':title')) {
        $token = 'title';
        $field_name = substr($name, 0, $pos);
      }
      if (!empty($token) && !empty($data['node']->{$field_name}[LANGUAGE_NONE][0]['course_number'])) {
        $course_number = $data['node']->{$field_name}[LANGUAGE_NONE][0]['course_number'];
        $replacements[$original] = _hfccwsclient_course_title_replacement($token, $course_number);
      }

      // Process field_person_hank_id tokens.

      elseif ($name == 'field_person_hank_fullname') {
        if (!empty($data['node']->field_person_hank_id)) {
          $hank_id = $data['node']->field_person_hank_id[LANGUAGE_NONE][0]['value'];
          if ($staffdir = WebServicesClient::getStaffdir(array('id=' . $hank_id))) {
            $staffdir = reset($staffdir);
            $fullname = array();
            foreach (array('firstname', 'lastname') as $field_name) {
              if (!empty($staffdir[$field_name])) {
                $fullname[] = trim($staffdir[$field_name]);
              }
            }
            $replacements[$original] = !empty($fullname) ? implode(' ', $fullname) : t('NAME NOT FOUND FOR @id', ['@id' => $hank_id]);
          }
          else {
            watchdog('mysite', 'Could not load staffdir info for @id in token @name', ['@id' => $hank_id, '@name' => $name], WATCHDOG_WARNING);
            $replacements[$original] = t('@id PERSON NOT FOUND', ['@id' => $hank_id]);
          }
        }
      }
      elseif ($name == 'field_person_hank_username') {
        if (!empty($data['node']->field_person_hank_id)) {
          $hank_id = $data['node']->field_person_hank_id[LANGUAGE_NONE][0]['value'];
          if ($staffdir = WebServicesClient::getStaffdir(array('id=' . $hank_id))) {
            $staffdir = reset($staffdir);
            $replacements[$original] = $staffdir['username'];
          }
          else {
            watchdog('mysite', 'Could not load staffdir info for @id in token @name', ['@id' => $hank_id, '@name' => $name], WATCHDOG_WARNING);
          }
        }
      }
    }
  }
  return $replacements;
}

/**
 * Gets the token replacement for course number_title and title tokens.
 *
 * @param string $token
 *   The token type to format.
 * @param string $course_number
 *   The course number to load.
 *
 * @return string
 *   The formatted token replacement.
 */
function _hfccwsclient_course_title_replacement($token, $course_number) {
  list($subject, $number) = explode('-', $course_number);
  if ($courses = WebServicesClient::getCourses(['subject=' . $subject, 'number=' . $number])) {
    $course = $courses[$course_number];
    switch ($token) {
      case 'number_title':
        return t('@subject @number @title', [
          '@subject' => $course['subject'],
          '@number' => $course['number'],
          '@title' => $course['title'],
        ]);
        break;
      default:
        return check_plain($course['title']);
    }
  }
  else {
    return t('Cannot retrieve title for @num', ['@num' => drupal_strtoupper($course_number)]);
  }
}
