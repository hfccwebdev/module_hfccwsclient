<?php

/**
 * @file
 * Contains the Web Services Client Testing Form.
 */

/**
 * Module testing form.
 */
function hfccwsclient_testing_form($form, &$form_state) {

  $form[] = [
    '#type' => 'item',
    '#markup' => t('Test HFC Web Services.'),
  ];

  $form[] = [
    '#type' => 'item',
    '#markup' => t('Current semester: @term', ['@term' => WebServicesClient::create()->getCurrentTerm()]),
  ];

  $options = [
    'divisions' => t('Divisions'),
    'departments' => t('Departments'),
    'buildings' => t('Buildings'),
    'subjects' => t('Subjects'),
    'terms' => t('Terms'),
    'courses' => t('Courses'),
    'programs' => t('Programs'),
    'staffdir' => t('Staff Directory'),
    'offices' => t('Offices'),
  ];

  $form['endpoint'] = [
    '#type' => 'radios',
    '#title' => t('Service to test'),
    '#options' => $options,
    '#required' => TRUE,
    '#description' => t('Select the services endpoint to test.'),
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Run test'),
  ];

  if (!empty($form_state['values']['endpoint'])) {
    $form['results'] = [
      '#type' => 'item',
      '#title' => t('Results'),
      '#prefix' => '<div>',
      'content' =>  WebServicesClientTests::test($form_state['values']['endpoint']),
      '#suffix' => '</div>',
    ];
  }
  return $form;
}

/**
 * Testing form submit.
 */
function hfccwsclient_testing_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

