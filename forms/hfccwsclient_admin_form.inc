<?php

/**
 * @file
 * Contains the Web Services Client Administration Form.
 */

/**
 * Module settings form.
 */
function hfccwsclient_admin_form($form, &$form_state) {

  $form[] = [
    '#type' => 'item',
    '#markup' => t('Administration page for HFC Web Services.'),
  ];
  $form['hfccwsclient_api_url'] = [
    '#type' => 'textfield',
    '#title' => t('Services API address'),
    '#default_value' => variable_get('hfccwsclient_api_url', NULL),
    '#description' => t('Enter the Base URL path to the services API. (ex: http://www.example.com/api/1.0)'),
  ];
  $form['hfccwsclient_redirect_base'] =[
    '#type' => 'textfield',
    '#title' => t('Catalog redirect base'),
    '#default_value' => variable_get('hfccwsclient_redirect_base', NULL),
    '#description' => t('Enter the Base URL path for Catalog redirects. (ex: https://catalog.example.com)'),
  ];
  $form['hfccwsclient_staffdir_div'] = [
    '#type' => 'select',
    '#title' => t('Default Division'),
    '#options' => WebServicesClient::getDivisionsOpts(TRUE),
    '#default_value' => variable_get('hfccwsclient_staffdir_div', NULL),
    '#description' => t('Select the division to be displayed in staff listings. (Does not affect the main staff directory listing.)'),
  ];
  $form['hfccwsclient_hank_photos'] = [
    '#type' => 'textfield',
    '#title' => t('HANK Photos URL'),
    '#default_value' => variable_get('hfccwsclient_hank_photos', NULL),
    '#description' => t('Enter the base URL for HANK Photos'),
  ];

  return system_settings_form($form);
}
