<?php

/**
 * @file
 * Contains the Web Services Staff Directory Form.
 */

/**
 * Page callback for the staff directory.
 */
function hfccwsclient_staffdir_form($form, &$form_state) {

  $form['search'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search options'),
    '#attributes' => array('class' => array('clearfix')),
  );

  $form['search']['lastname'] = array(
    '#type' => 'textfield',
    '#title' => t('Last name'),
    '#size' => 40,
    '#default_value' => !empty($form_state['values']['lastname']) ? $form_state['values']['lastname'] : NULL,
  );

  $form['search']['firstname'] = array(
    '#type' => 'textfield',
    '#title' => t('First name'),
    '#size' => 40,
    '#default_value' => !empty($form_state['values']['firstname']) ? $form_state['values']['firstname'] : NULL,
  );

  $form['search']['division'] = array(
    '#type' => 'select',
    '#title' => t('Division'),
    '#options' => StaffdirTools::getActiveDivisionOpts(TRUE),
    '#default_value' => !empty($form_state['values']['division']) ? $form_state['values']['division'] : NULL,
  );

  $form['search']['department'] = array(
    '#type' => 'select',
    '#title' => t('Department'),
    '#options' => StaffdirTools::getActiveDepartmentOpts(TRUE),
    '#default_value' => !empty($form_state['values']['department']) ? $form_state['values']['department'] : NULL,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );
  if (!empty($form_state['values']['results'])) {
    $form['results'] = array(
      '#type' => 'item',
      '#title' => t('Results'),
      '#markup' => $form_state['values']['results'],
      '#attributes' => array('class' => array('results')),
    );
  }
  return $form;
}

/**
 * Staff Directory form submit.
 */
function hfccwsclient_staffdir_form_submit($form, &$form_state) {
  $filter_opts = array();
  if (!empty($form_state['values']['lastname'])) {
    $filter_opts[] = 'lastname=' . $form_state['values']['lastname'];
  }
  if (!empty($form_state['values']['firstname'])) {
    $filter_opts[] = 'firstname=' . $form_state['values']['firstname'];
  }
  if (!empty($form_state['values']['division']) && $form_state['values']['division'] !== 'All') {
    $filter_opts[] = 'division=' . $form_state['values']['division'];
  }
  if (!empty($form_state['values']['department']) && $form_state['values']['department'] !== 'All') {
    $filter_opts[] = 'department=' . $form_state['values']['department'];
  }

  $staffdir = WebServicesClient::getStaffdir($filter_opts);

  $rows = array();
  foreach ($staffdir as $person) {
    $name = array();
    $last_name = !empty($person['lastname']) ? $person['lastname'] : '';
    $first_name = !empty($person['firstname']) ? $person['firstname'] : '';
    $name[] = '<strong>' . $last_name . ', ' . $first_name . '</strong>';
    if (!empty($person['title'])) {
      $name[] = $person['title'];
    }

    $divdept = array();
    if (!empty($person['division'])) {
      $element = array('division' => $person['division'], '#theme' => 'hank_division');
      $divdept[] = '<strong>' . render($element) . '</strong>';
    }
    if (!empty($person['department'])) {
      $element = array('department' => $person['department'], '#theme' => 'hank_department');
      $divdept[] = render($element);
    }

    $location = array();
    if (!empty($person['building'])) {
      $element = array('building' => $person['building'], '#theme' => 'hank_building');
      $location[] = '<strong>' . render($element) . '</strong>';
    }
    if (!empty($person['office_location'])) {
      $location[] = $person['office_location'];
    }

    $contact = array();
    if (!empty($person['email_address'])) {
      $contact[] = '<strong>' . $person['email_address'] . '</strong>';
    }
    if (!empty($person['phone_number'])) {
      $contact[] = check_plain($person['phone_number']);
    }

    $rows[] = array(
      implode('<br/>', $name),
      implode('<br/>', $divdept),
      implode('<br/>', $location),
      implode('<br/>', $contact),
    );
  }
  $output = array(
    '#theme' => 'table',
    '#header' => array(
      t('Name'),
      t('Division / Department'),
      t('Office Location'),
      t('Contact Information'),
    ),
    '#rows' => $rows,
    '#empty' => t('No results returned'),
    '#attributes' => array('class' => array('staff-directory-results')),
  );

  $form_state['values']['results'] = drupal_render($output);
  $form_state['rebuild'] = TRUE;

}

