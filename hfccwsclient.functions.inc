<?php

/**
 * @file
 * Contains data retrieval functions for the HFC Web Services Client.
 *
 * @see hfccwsclient.module
 */

/**
 * Retrieve a URL from the services host.
 *
 * @deprecated in hfccwsclient-7.x-2.x and will be removed. Use WebServicesClient::httpRequest() instead.
 */
function hfccwsclient_http_request($resource, $filter_opts = array()) {
  return WebServicesClient::httpRequest($resource, $filter_opts);
}

/**
 * Retrieve a list of buildings from the services host.
 *
 * @deprecated in hfccwsclient-7.x-2.x and will be removed. Use WebServicesClient::getBuildings() instead.
 */
function hfccwsclient_get_buildings() {
  return WebServicesClient::getBuildings();
}

/**
 * Retrieve buildings as a list of options.
 *
 * @deprecated in hfccwsclient-7.x-2.x and will be removed. Use WebServicesClient::getBuildingsOpts() instead.
 */
function hfccwsclient_get_buildings_opts($allow_all = FALSE) {
  return WebServicesClient::getBuildingsOpts($allow_all);
}

/**
 * Retrieve catalog courses from the services host.
 *
 * @deprecated in hfccwsclient-7.x-2.x and will be removed. Use WebServicesClient::getCourses() instead.
 */
function hfccwsclient_get_courses($filter_opts = array()) {
  return WebServicesClient::getCourses($filter_opts);
}

/**
 * Retrieve courses as a list of options.
 *
 * @deprecated in hfccwsclient-7.x-2.x and will be removed. Use WebServicesClient::getCoursesOpts() instead.
 */
function hfccwsclient_get_courses_opts($filter_opts = array()) {
  return WebServicesClient::getCoursesOpts($filter_opts);
}

/**
 * Retrieve a list of departments from the services host.
 *
 * @deprecated in hfccwsclient-7.x-2.x and will be removed. Use WebServicesClient::getDepartments() instead.
 */
function hfccwsclient_get_departments() {
  return WebServicesClient::getDepartments();
}

/**
 * Retrieve departments as a list of options.
 *
 * @deprecated in hfccwsclient-7.x-2.x and will be removed. Use WebServicesClient::getDepartmentsOpts() instead.
 */
function hfccwsclient_get_departments_opts($allow_all = FALSE) {
  return WebServicesClient::getDepartmentsOpts($allow_all);
}

/**
 * Retrieve a list of divisions from the services host.
 *
 * @deprecated in hfccwsclient-7.x-2.x and will be removed. Use WebServicesClient::getDivisions() instead.
 */
function hfccwsclient_get_divisions() {
  return WebServicesClient::getDivisions();
}

/**
 * Retrieve divisions as a list of options.
 *
 * @deprecated in hfccwsclient-7.x-2.x and will be removed. Use WebServicesClient::getDivisionsOpts() instead.
 */
function hfccwsclient_get_divisions_opts($allow_all = FALSE) {
  return WebServicesClient::getDivisionsOpts($allow_all);
}

/**
 * Retrieve ID photo URL.
 *
 * @todo Find where this is used and replace with hfc_hank_api module.
 */
function hfccwsclient_get_idphoto($hankid) {
  $hankphotos = variable_get('hfccwsclient_hank_photos', NULL);
  if (empty($hankphotos)) {
    drupal_set_message(t('Please set base URL for HANK photos.'), 'warning');
    return NULL;
  }
  $idphoto = check_url($hankphotos . '/' . drupal_substr('0000000' . $hankid, -7) . '.jpg');
  return file_exists($idphoto) ? $idphoto : NULL;
}

/**
 * Retrieve offices from the services host.
 *
 * @deprecated in hfccwsclient-7.x-2.x and will be removed. Use WebServicesClient::getOffices() instead.
 */
function hfccwsclient_get_offices($filter_opts = array()) {
  return WebServicesClient::getOffices($filter_opts);
}

/**
 * Retrieve offices as a list of options.
 *
 * @deprecated in hfccwsclient-7.x-2.x and will be removed. Use WebServicesClient::getOfficesOpts() instead.
 */
function hfccwsclient_get_offices_opts($allow_all = FALSE) {
  return WebServicesClient::getOfficesOpts($allow_all);
}

/**
 * Determine today's office hours.
 *
 * @deprecated in hfccwsclient-7.x-2.x and will be removed. Use WebServicesClient::getOfficeHoursToday() instead.
 */
function hfccwsclient_office_hours_today($office) {
  return WebServicesClient::getOfficeHoursToday($office);
}

/**
 * Retrieve catalog programs from the services host.
 *
 * @deprecated in hfccwsclient-7.x-2.x and will be removed. Use WebServicesClient::getPrograms() instead.
 */
function hfccwsclient_get_programs() {
  return WebServicesClient::getPrograms();
}

/**
 * Retrieve catalog program degree types from the services host.
 *
 * @deprecated in hfccwsclient-7.x-2.x and will be removed. Use WebServicesClient::getProgramTypes() instead.
 */
function hfccwsclient_get_program_types() {
  return WebServicesClient::getProgramTypes();
}

/**
 * Retrieve Programs as a list of options.
 *
 * @deprecated in hfccwsclient-7.x-2.x and will be removed. Use WebServicesClient::getProgramsOpts() instead.
 */
function hfccwsclient_get_programs_opts($allow_all = FALSE) {
  return WebServicesClient::getProgramsOpts($allow_all);
}

/**
 * Retrieve a list of subjects from the services host.
 *
 * @deprecated in hfccwsclient-7.x-2.x and will be removed. Use WebServicesClient::getSubjects() instead.
 */
function hfccwsclient_get_subjects() {
  return WebServicesClient::getSubjects();
}

/**
 * Retrieve staff directory from the services host.
 *
 * @deprecated in hfccwsclient-7.x-2.x and will be removed. Use WebServicesClient::getStaffdir() instead.
 */
function hfccwsclient_get_staffdir($filter_opts = array()) {
  return WebServicesClient::getStaffdir($filter_opts);
}

/**
 * Retrieve staff directory by HANK IDs as options list.
 *
 * @deprecated in hfccwsclient-7.x-2.x and will be removed. Use WebServicesClient::getStaffdirHankidOpts() instead.
 */
function hfccwsclient_get_staffdir_hankid_opts($filter_opts = array()) {
  return WebServicesClient::getStaffdirHankidOpts($filter_opts);
}

/**
 * Retrieve a list of academic terms from the services host.
 *
 * @deprecated in hfccwsclient-7.x-2.x and will be removed. Use WebServicesClient::getTerms() instead.
 */
function hfccwsclient_get_terms() {
  return WebServicesClient::getTerms();
}

/**
 * Retrieve terms as a list of options.
 *
 * @deprecated in hfccwsclient-7.x-2.x and will be removed. Use WebServicesClient::getTermsOpts() instead.
 */
function hfccwsclient_get_terms_opts($filter_opts = array(), $allow_all = FALSE) {
  return WebServicesClient::getTermsOpts($filter_opts, $allow_all);
}

/**
 * Retrieve terms as a list of options.
 *
 * @deprecated in hfccwsclient-7.x-2.x and will be removed. Use WebServicesClient::getCurrentTerm() instead.
 */
function hfccwsclient_get_current_term() {
  return WebServicesClient::getCurrentTerm();
}

/**
 * Format office hours times.
 * @see office_hours.module
 *
 * @deprecated in hfccwsclient-7.x-2.x and will be removed. Use WebServicesClient::formatTime() instead.
 */
function _hfccwsclient_format_time($time) {
  return WebServicesClient::formatTime($time);
}
